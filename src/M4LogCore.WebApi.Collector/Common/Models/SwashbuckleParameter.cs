﻿using System.Collections.Generic;
using Swashbuckle.Swagger.Model;

namespace M4LogCore.WebApi.Collector.Common.Models
{
    public class SwashbuckleParameter : PartialSchema, IParameter
    {
        public string Description { get; set; }
        public Dictionary<string, object> Extensions { get; }
        public string In { get; set; }

        public string Name { get; set; }
        public bool Required { get; set; }
    }
}