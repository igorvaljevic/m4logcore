﻿using System;
using System.Net;
using System.Threading.Tasks;
using M4LogCore.Logic.Common.Enums;
using M4LogCore.Logic.Common.Interfaces;
using M4LogCore.Logic.Repository.Projects.Queries;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;

namespace M4LogCore.WebApi.Collector.Common.Middlewares
{
    public class RequestProjectKeyHeaderValidator
    {
        private readonly ILogger _logger;
        private readonly RequestDelegate _next;
        private readonly IQueryHandler<ProjectKeyValidateQuery, bool> _projectKeyValidator;

        public RequestProjectKeyHeaderValidator(
            ILogger<GlobalHttpRequestErrorLoggingMiddleware> logger,
            IQueryHandler<ProjectKeyValidateQuery, bool> projectKeyValidator,
            RequestDelegate next)
        {
            _next = next;
            _logger = logger;
            _projectKeyValidator = projectKeyValidator;
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                string userKey = context.Request.Headers["userKey"];
                string projectKey = context.Request.Headers["projectKey"];
                var query = new ProjectKeyValidateQuery
                {
                    UseCache = true,
                    CacheKey = userKey + projectKey,
                    ProjectKey = projectKey,
                    UserKey = userKey
                };
                var isValid = _projectKeyValidator.Handle(query);
                if (isValid)
                {
                    await _next(context);
                }
                else
                {
                    _logger.LogError((int)LoggingEvents.PreRequestProcessing,
                        $"Project Key is missing or invalid. {nameof(query.UserKey)}[{query.UserKey}]; {nameof(query.ProjectKey)}[{query.ProjectKey}]");
                    context.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                    await context.Response.WriteAsync("Project Key is missing or invalid.");
                }
            }
            catch (Exception ex)
            {
                _logger.LogError((int)LoggingEvents.PreRequestProcessing, ex, $"The following error happened: {ex.Message}");
            }
        }
    }
}