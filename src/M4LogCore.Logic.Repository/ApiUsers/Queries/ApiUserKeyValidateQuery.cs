﻿using M4LogCore.Logic.Common.Interfaces;

namespace M4LogCore.Logic.Repository.ApiUsers.Queries
{
    public class ApiUserKeyValidateQuery : IQuery<bool>
    {
        public string CacheKey { get; set; }
        public bool UseCache { get; set; }
        public string Key { get; set; }
    }
}