﻿using System;
using System.Collections.Generic;
using M4LogCore.Logic.Common;
using M4LogCore.Logic.Common.Interfaces;
using Microsoft.Extensions.Options;

namespace M4LogCore.Logic.Repository.Decorators
{
    public class CacheValidatorDecorator<TQuery, TResult> : IQueryHandler<TQuery, TResult> where TQuery : IQuery<TResult>
    {
        private readonly IQueryHandler<TQuery, TResult> _decorated;
        private IDictionary<string, CacheItem> _cache;
        private TimeSpan _cacheItemTimeToLive;
        private static object _lockKey = new object();

        public CacheValidatorDecorator(IOptions<CacheSettings> settings, IQueryHandler<TQuery, TResult> decorated)
        {
            _decorated = decorated;
            _cache = new Dictionary<string, CacheItem>();
            _cacheItemTimeToLive = settings.Value.CacheItemTimeToLive;
        }

        public TResult Handle(TQuery query)
        {
            TResult result;
            if (!query.UseCache || String.IsNullOrEmpty(query.CacheKey))
            {
                result = _decorated.Handle(query);
            }
            else
            {
                if (_cache.ContainsKey(query.CacheKey) && _cache[query.CacheKey].Expiration > DateTime.Now)
                {
                    result = _cache[query.CacheKey].Result;
                }
                else
                {
                    result = _decorated.Handle(query);
                    lock (_lockKey)
                    {
                        if (!(_cache.ContainsKey(query.CacheKey) && _cache[query.CacheKey].Expiration > DateTime.Now))
                        {
                            _cache.Remove(query.CacheKey);
                            _cache.Add(query.CacheKey, new CacheItem { Result = result, Expiration = DateTime.Now.Add(_cacheItemTimeToLive) });
                        }
                    }
                }
            }
            return result;
        }
        private class CacheItem
        {
            public TResult Result { get; set; }
            public DateTime Expiration { get; set; }
        }
    }
}