﻿using System.Collections.Generic;
using M4LogCore.Logic.Common.Interfaces.Models;

namespace M4LogCore.WebApi.Collector.Models
{
    public class M4LoggingEventDto : IM4LoggingEvent
    {
        public string Domain { get; set; }
        public string ExceptionObject { get; set; }
        public string Fix { get; set; }
        public string Identity { get; set; }
        public int Level { get; set; }
        public string LocationInformation { get; set; }
        public string LoggerName { get; set; }
        public string MessageObject { get; set; }
        public string ProjectKey { get; set; }
        public IEnumerable<KeyValuePair<string, string>> Properties { get; set; }
        public string RenderedMessage { get; set; }
        public string Repository { get; set; }
        public string ThreadName { get; set; }
        public string TimeStamp { get; set; }
        public string UserName { get; set; }
    }
}