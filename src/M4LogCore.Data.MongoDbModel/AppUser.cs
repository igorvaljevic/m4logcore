﻿using System.Linq;
using M4LogCore.Logic.Common.Interfaces.Models;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;

namespace M4LogCore.Data.MongoDbRepositories
{
    public class AppUser : IAppUser, IMongoEntity
    {
        public ObjectId _id;
        private static string _tableName = "AppUser";
        public string ApiKey { get; set; }

        [BsonIgnore]
        public string TableName
        {
            get
            {
                return _tableName;
            }
        }

        public string UserName { get; set; }

        internal static IQueryable<IAppUser> GetCollection(IMongoDatabase database)
        {
            var users = database.GetCollection<AppUser>(_tableName);

            return users.AsQueryable();
        }

        internal static void InitCollection(IMongoDatabase database)
        {
            var users = database.GetCollection<AppUser>(_tableName);

            users.Indexes.CreateOne(Builders<AppUser>.IndexKeys.Ascending(_ => _.ApiKey), new CreateIndexOptions { Unique = true });
            users.Indexes.CreateOne(Builders<AppUser>.IndexKeys.Ascending(_ => _.UserName), new CreateIndexOptions { Unique = true });
        }
    }
}