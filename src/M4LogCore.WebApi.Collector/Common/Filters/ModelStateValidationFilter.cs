﻿using System;
using M4LogCore.Logic.Common.Enums;
using M4LogCore.WebApi.Collector.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;

namespace M4LogCore.WebApi.Collector.Common.Filters
{
    public class ModelStateValidationFilter : Attribute, IActionFilter
    {
        private ILogger _logger;

        public ModelStateValidationFilter(ILogger<ModelStateValidationFilter> logger)
        {
            _logger = logger;
        }

        public void OnActionExecuted(ActionExecutedContext context)
        {
        }

        public void OnActionExecuting(ActionExecutingContext context)
        {
            if (!context.ModelState.IsValid)
            {
                foreach (var model in context.ModelState)
                {
                    foreach (var error in model.Value?.Errors)
                    {
                        _logger.LogError((int)LoggingEvents.ModelBinding, error.Exception, $"Model binding failed for [{model.Key}]");
                    }
                }
                context.Result = new BadRequestObjectResult(context.ModelState);
            }
        }
    }
}