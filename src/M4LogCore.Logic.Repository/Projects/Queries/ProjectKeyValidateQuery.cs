﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using M4LogCore.Logic.Common.Interfaces;

namespace M4LogCore.Logic.Repository.Projects.Queries
{
    public class ProjectKeyValidateQuery : IQuery<bool>
    {
        public string CacheKey { get; set; }
        public bool UseCache { get; set; }
        public string ProjectKey { get; set; }
        public string UserKey { get; set; }
    }
}
