﻿using System;
using System.Net;
using System.Threading.Tasks;
using M4LogCore.Logic.Common.Enums;
using M4LogCore.Logic.Common.Interfaces;
using M4LogCore.Logic.Repository.ApiUsers.Queries;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;

namespace M4LogCore.WebApi.Collector.Common.Middlewares
{
    public class RequestApiKeyHeaderValidator
    {
        private readonly IQueryHandler<ApiUserKeyValidateQuery, bool> _apiUserKeyValidator;
        private readonly ILogger _logger;
        private readonly RequestDelegate _next;

        public RequestApiKeyHeaderValidator(
            ILogger<GlobalHttpRequestErrorLoggingMiddleware> logger,
            IQueryHandler<ApiUserKeyValidateQuery, bool> apiUserKeyValidator,
            RequestDelegate next)
        {
            _next = next;
            _logger = logger;
            _apiUserKeyValidator = apiUserKeyValidator;
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                string userKey = context.Request.Headers["userKey"];
                var query = new ApiUserKeyValidateQuery
                {
                    UseCache = true,
                    CacheKey = userKey,
                    Key = userKey
                };
                var isValid = _apiUserKeyValidator.Handle(query);
                if (isValid)
                {
                    await _next(context);
                }
                else
                {
                    _logger.LogError((int)LoggingEvents.PreRequestProcessing, $"User Key is missing or invalid: [{query.Key}]");
                    context.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                    await context.Response.WriteAsync("User Key is missing or invalid.");
                }
            }
            catch (Exception ex)
            {
                _logger.LogError((int)LoggingEvents.PreRequestProcessing, ex, $"The following error happened: {ex.Message}");
            }
        }
    }
}