﻿using M4LogCore.WebApi.Collector.Common.Filters;
using Microsoft.AspNetCore.Mvc;

namespace M4LogCore.WebApi.Collector.Controllers
{
    [ServiceFilter(typeof(ModelStateValidationFilter))]
    public class BaseController : Controller
    {
    }
}