﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace M4LogCore.Data.MongoDbRepositories
{
    public interface IMongoEntity
    {
        string TableName { get; }
    }
}
