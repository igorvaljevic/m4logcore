﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace M4LogCore.Logic.Common.Interfaces.Models
{
    public interface IAppUser
    {
        string ApiKey { get; set; }
        string UserName { get; set; }
    }
}
