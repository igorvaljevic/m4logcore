﻿using System.Linq;
using M4LogCore.Logic.Common;
using M4LogCore.Logic.Common.Interfaces.Models;
using MongoDB.Driver;

namespace M4LogCore.Data.MongoDbRepositories
{
    public interface IDbContext
    {
        IQueryable<IAppUser> Users { get; }
        void Insert<T>(T item) where T : IMongoEntity;
    }
}