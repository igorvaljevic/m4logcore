﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace M4LogCore.Logic.Common
{
    public class MongoSettings
    {
        public string ConnectionString;
        public string Database;
        public bool InitCollections;
    }
}
