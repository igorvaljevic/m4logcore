﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace M4LogCore.Logic.Common
{
    public class CacheSettings
    {
        public TimeSpan CacheItemTimeToLive { get; set; }
    }
}
