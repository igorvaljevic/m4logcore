﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using M4LogCore.WebApi.Collector.Common.Models;
using Swashbuckle.Swagger.Model;
using Swashbuckle.SwaggerGen.Generator;

namespace M4LogCore.WebApi.Collector.Common.SwaggerExtensions
{
    public class UserKeyHeaderParameterOperationFilter : IOperationFilter
    {
        public void Apply(Operation operation, OperationFilterContext context)
        {
            operation.Parameters.Add(new SwashbuckleParameter
            {
                Name = "userKey",
                In = "header",
                Description = "UserKey used to authenticate access to the API",
                Required = true,
                Type = "string",
#if DEBUG
                Default = "key1",
#endif
            });
            operation.Parameters.Add(new SwashbuckleParameter
            {
                Name = "projectKey",
                In = "header",
                Description = "ProjectKey used to authenticate access to the API",
                Required = true,
                Type = "string"
            });
        }
    }
}
