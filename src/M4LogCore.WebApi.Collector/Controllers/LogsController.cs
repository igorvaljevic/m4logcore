﻿using System.Collections.Generic;
using M4LogCore.Logic.Common.Interfaces;
using M4LogCore.Logic.Repository.Logs.Commands;
using M4LogCore.WebApi.Collector.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace M4LogCore.WebApi.Collector.Controllers
{
    [Route("api/[controller]")]
    public class LogsController : BaseController
    {
        ICommandHandler<InsertLogMessagesCommand> _handler;
        public LogsController(ICommandHandler<InsertLogMessagesCommand> handler)
        {
            _handler = handler;
        }
        // POST api/logs
        [HttpPost]
        public IActionResult Post([FromBody]IEnumerable<M4LoggingEventDto> value)
        {
            var command = new InsertLogMessagesCommand { Messages = value };
            _handler.Handle(command);
            return Ok(value);
        }
    }
}