﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using M4LogCore.Logic.Common;
using M4LogCore.Logic.Common.Interfaces.Models;
using Microsoft.Extensions.Options;
using MongoDB.Driver;

namespace M4LogCore.Data.MongoDbRepositories
{
    public class MongoDbContext : IDbContext
    {
        private readonly IMongoDatabase _database;

        private readonly IQueryable<IAppUser> _users;

        public MongoDbContext(IMongoDbServer dbServer)
        {
            _database = dbServer.Database;

            _users = AppUser.GetCollection(_database);
        }

        public IQueryable<IAppUser> Users
        {
            get { return _users; }
        }

        public void Delete<T>(Expression<Func<T, bool>> filter) where T : IMongoEntity, new()
        {
            var virtualItem = new T();
            var collection = _database.GetCollection<T>(virtualItem.TableName);
            collection.DeleteMany(filter);
        }

        public Task DeleteAsync<T>(Expression<Func<T, bool>> filter) where T : IMongoEntity, new()
        {
            var virtualItem = new T();
            var collection = _database.GetCollection<T>(virtualItem.TableName);
            return collection.DeleteManyAsync(filter);
        }

        public void Insert<T>(T item) where T : IMongoEntity
        {
            var collection = _database.GetCollection<T>(item.TableName);
            collection.InsertOne(item);
        }

        public void Insert<T>(IOrderedEnumerable<T> itemCollection) where T : IMongoEntity
        {
            var anyItem = itemCollection.First();
            var collection = _database.GetCollection<T>(anyItem.TableName);
            collection.InsertMany(itemCollection);
        }

        public Task InsertAsync<T>(IOrderedEnumerable<T> itemCollection) where T : IMongoEntity
        {
            var anyItem = itemCollection.First();
            var collection = _database.GetCollection<T>(anyItem.TableName);
            return collection.InsertManyAsync(itemCollection);
        }
    }
}