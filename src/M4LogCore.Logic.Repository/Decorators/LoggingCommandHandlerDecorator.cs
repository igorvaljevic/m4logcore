﻿using M4LogCore.Logic.Common.Interfaces;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace M4LogCore.Logic.Repository.Decorators
{
    public class LoggingCommandHandlerDecorator<TCommand> : ICommandHandler<TCommand>
    {
        private readonly ICommandHandler<TCommand> _decorated;
        private readonly ILogger _logger;

        public LoggingCommandHandlerDecorator(ICommandHandler<TCommand> decorated, ILogger<LoggingCommandHandlerDecorator<TCommand>> logger)
        {
            _decorated = decorated;
            _logger = logger;
        }

        public void Handle(TCommand command)
        {
            _logger.LogDebug($"Handling command: [{command.GetType().Name}]. {JsonConvert.SerializeObject(command)}");

            _decorated.Handle(command);

            _logger.LogDebug($"[{command.GetType().Name}] done.");
        }
    }
}