﻿namespace M4LogCore.Logic.Common.Interfaces.Models
{
    public interface IProject
    {
        int Id { get; set; }
        string Key { get; set; }
        string Name { get; set; }
    }
}