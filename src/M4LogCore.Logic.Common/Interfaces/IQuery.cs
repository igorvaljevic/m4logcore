﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace M4LogCore.Logic.Common.Interfaces
{
    public interface IQuery<TResult>
    {
        string CacheKey { get; set; }
        bool UseCache { get; set; }
    }
}
