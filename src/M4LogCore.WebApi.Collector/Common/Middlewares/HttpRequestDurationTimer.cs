﻿using System;
using System.Globalization;
using System.Threading.Tasks;
using M4LogCore.Logic.Common.Enums;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;

namespace M4LogCore.WebApi.Collector.Common.Middlewares
{
    public class HttpRequestDurationTimer
    {
        private readonly ILogger _logger;
        private readonly RequestDelegate _next;

        public HttpRequestDurationTimer(ILogger<GlobalHttpRequestErrorLoggingMiddleware> logger, RequestDelegate next)
        {
            _next = next;
            _logger = logger;
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                var startTime = DateTime.Now;
                _logger.LogDebug($"Request started at {startTime.ToString("yyyy-MM-ddTHH:mm:ss.fff", CultureInfo.InvariantCulture)}.");

                await _next(context);

                var endTime = DateTime.Now;
                _logger.LogDebug($"Request finished at {endTime.ToString("yyyy-MM-ddTHH:mm:ss.fff", CultureInfo.InvariantCulture)}.");
                _logger.LogDebug($"Request duration: [{(endTime - startTime).TotalMilliseconds}]");
            }
            catch (Exception ex)
            {
                _logger.LogError((int)LoggingEvents.PreRequestProcessing, ex, $"The following error happened: {ex.Message}");
            }
        }
    }
}