﻿using System.Linq;
using M4LogCore.Data.MongoDbRepositories;
using M4LogCore.Logic.Common.Interfaces;

namespace M4LogCore.Logic.Repository.ApiUsers.Queries
{
    public class ApiUserKeyValidateQueryHandler : IQueryHandler<ApiUserKeyValidateQuery, bool>
    {
        private IDbContext _dbContext;

        public ApiUserKeyValidateQueryHandler(IDbContext context)
        {
            _dbContext = context;
        }

        public bool Handle(ApiUserKeyValidateQuery query)
        {
            var isValid = _dbContext.Users.Where(_ => _.ApiKey == query.Key).Any();

            return isValid;
        }
    }
}