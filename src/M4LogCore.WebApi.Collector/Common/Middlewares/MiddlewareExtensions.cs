﻿using Microsoft.AspNetCore.Builder;

namespace M4LogCore.WebApi.Collector.Common.Middlewares
{
    public static class MiddlewareExtensions
    {
        public static IApplicationBuilder UseGlobalHttpRequestErrorLogging(this IApplicationBuilder app)
        {
            return app.UseMiddleware<GlobalHttpRequestErrorLoggingMiddleware>();
        }

        public static IApplicationBuilder UseRequestApiKeyHeaderAuthentication(this IApplicationBuilder app)
        {
            return app.UseMiddleware<RequestApiKeyHeaderValidator>();
        }

        public static IApplicationBuilder UseProjectKeyHeaderAuthentication(this IApplicationBuilder app)
        {
            return app.UseMiddleware<RequestProjectKeyHeaderValidator>();
        }

        public static IApplicationBuilder UseHttpRequestDurationTimer(this IApplicationBuilder app)
        {
            return app.UseMiddleware<HttpRequestDurationTimer>();
        }
    }
}