﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace M4LogCore.Logic.Common.Enums
{
    public enum LoggingEvents : int
    {
        ModelBinding = 1000,
        PreRequestProcessing = 1001
    }
}
