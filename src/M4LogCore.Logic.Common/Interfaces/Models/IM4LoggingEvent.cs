﻿using System.Collections.Generic;

namespace M4LogCore.Logic.Common.Interfaces.Models
{
    public interface IM4LoggingEvent
    {
        string Domain { get; set; }
        string ExceptionObject { get; set; }
        string Fix { get; set; }
        string Identity { get; set; }
        int Level { get; set; }
        string LocationInformation { get; set; }
        string LoggerName { get; set; }
        string MessageObject { get; set; }
        IEnumerable<KeyValuePair<string, string>> Properties { get; set; }
        string RenderedMessage { get; set; }
        string Repository { get; set; }
        string ThreadName { get; set; }
        string TimeStamp { get; set; }
        string UserName { get; set; }
    }
}