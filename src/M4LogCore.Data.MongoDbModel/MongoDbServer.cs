﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using M4LogCore.Logic.Common;
using Microsoft.Extensions.Options;
using MongoDB.Driver;

namespace M4LogCore.Data.MongoDbRepositories
{
    public class MongoDbServer : IMongoDbServer
    {
        private readonly IMongoDatabase _database;
        public IMongoDatabase Database { get { return _database; } }
        public MongoDbServer(IOptions<MongoSettings> settings)
        {
            var client = new MongoClient(settings.Value.ConnectionString);
            _database = client?.GetDatabase(settings.Value.Database);
            if (_database == null)
            {
                throw new ArgumentNullException("Invalid connection string or database name.");
            }
            if (settings.Value.InitCollections)
            {
                InitCollections();
            }
        }

        private void InitCollections()
        {
            AppUser.InitCollection(_database);
        }
    }
}
