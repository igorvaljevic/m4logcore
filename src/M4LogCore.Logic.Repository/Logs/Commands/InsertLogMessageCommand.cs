﻿using System.Collections.Generic;
using M4LogCore.Logic.Common.Interfaces.Models;

namespace M4LogCore.Logic.Repository.Logs.Commands
{
    public class InsertLogMessagesCommand
    {
        public IEnumerable<IM4LoggingEvent> Messages { get; set; }
    }
}