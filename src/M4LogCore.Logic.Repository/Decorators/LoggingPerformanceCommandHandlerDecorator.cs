﻿using System;
using M4LogCore.Logic.Common.Interfaces;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace M4LogCore.Logic.Repository.Decorators
{
    public class LoggingPerformanceCommandHandlerDecorator<TCommand> : ICommandHandler<TCommand>
    {
        private readonly ICommandHandler<TCommand> _decorated;
        private readonly ILogger _logger;

        public LoggingPerformanceCommandHandlerDecorator(ICommandHandler<TCommand> decorated, ILogger<LoggingPerformanceCommandHandlerDecorator<TCommand>> logger)
        {
            _decorated = decorated;
            _logger = logger;
        }

        public void Handle(TCommand command)
        {
            var timeStart = DateTime.Now;

            _decorated.Handle(command);

            _logger.LogDebug($@"Execution time: {
                JsonConvert.SerializeObject(
                    new
                    {
                        Command = command.GetType().Name,
                        ExecutionTime = (DateTime.Now - timeStart).TotalMilliseconds
                    })}
            ");
        }
    }
}