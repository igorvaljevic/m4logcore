﻿using System;
using System.IO;
using M4LogCore.Data.MongoDbRepositories;
using M4LogCore.Logic.Common;
using M4LogCore.Logic.Common.Interfaces;
using M4LogCore.Logic.Common.Interfaces.Models;
using M4LogCore.Logic.Repository.Decorators;
using M4LogCore.WebApi.Collector.Common.Filters;
using M4LogCore.WebApi.Collector.Common.Middlewares;
using M4LogCore.WebApi.Collector.Common.SwaggerExtensions;
using M4LogCore.WebApi.Collector.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.PlatformAbstractions;
using Newtonsoft.Json.Serialization;
using StructureMap;
using Swashbuckle.Swagger.Model;

namespace M4LogCore.WebApi.Collector
{
    public class Startup
    {
        private ILogger _logger;

        public Startup(IHostingEnvironment env, ILogger<Startup> logger)
        {
            _logger = logger;
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));

            app.UseSwagger();
            app.UseSwaggerUi();

            app.UseGlobalHttpRequestErrorLogging();
            app.UseRequestApiKeyHeaderAuthentication();
            app.UseProjectKeyHeaderAuthentication();
            app.UseHttpRequestDurationTimer();

            app.UseMvc();
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            AddFilters(services);
            BindModels(services);

            services.AddRouting(options =>
            {
                options.LowercaseUrls = true;
            });

            services.Configure<MongoSettings>(options =>
            {
                options.ConnectionString = Configuration.GetSection("MongoConnection:ConnectionString").Value;
                options.Database = Configuration.GetSection("MongoConnection:Database").Value;
                options.InitCollections = Configuration.GetValue<bool>("MongoConnection:InitCollections");
            });
            services.Configure<CacheSettings>(options =>
            {
                options.CacheItemTimeToLive = TimeSpan.Parse(Configuration.GetSection("CacheSettings:CacheItemTimeToLive").Value);
            });

            // Add framework services.
            services.AddMvc().AddJsonOptions(options =>
            {
                options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            });
            /*Adding swagger generation with default settings*/
            services.AddSwaggerGen(options =>
            {
                options.SingleApiVersion(new Info
                {
                    Version = "v1",
                    Title = "Sample Rest API",
                    Description = "API Sample made for Auth0",
                    TermsOfService = "None"
                });

                options.DocumentFilter<LowercaseDocumentFilter>();
                options.OperationFilter<UserKeyHeaderParameterOperationFilter>();

                //Set the comments path for the swagger json and ui.
                var basePath = PlatformServices.Default.Application.ApplicationBasePath;
                var xmlPath = Path.Combine(basePath, "M4LogCore.WebApi.Collector.xml");
                options.IncludeXmlComments(xmlPath);
            });

            var container = new Container(_ =>
            {
                _.Scan(scan =>
                {
                    scan.Assembly("M4LogCore.Logic.Repository");
                    scan.ConnectImplementationsToTypesClosing(typeof(ICommandHandler<>));
                    //scan.ConnectImplementationsToTypesClosing(typeof(IQuery<>));
                    scan.ConnectImplementationsToTypesClosing(typeof(IQueryHandler<,>));
                });
                _.AddType(typeof(IDbContext), typeof(MongoDbContext));
                _.For(typeof(ICommandHandler<>)).DecorateAllWith(typeof(LoggingCommandHandlerDecorator<>));
                _.For(typeof(ICommandHandler<>)).DecorateAllWith(typeof(LoggingPerformanceCommandHandlerDecorator<>));
                _.For(typeof(IQueryHandler<,>)).DecorateAllWith(typeof(CacheValidatorDecorator<,>));
                _.For<IMongoDbServer>().Use<MongoDbServer>().ContainerScoped();
            });
            container.Populate(services);

            return container.GetInstance<IServiceProvider>();
        }

        private static void AddFilters(IServiceCollection services)
        {
            services.AddScoped<ModelStateValidationFilter>();
        }

        private static void BindModels(IServiceCollection services)
        {
            services.AddScoped<IM4LoggingEvent, M4LoggingEventDto>();
        }
    }
}