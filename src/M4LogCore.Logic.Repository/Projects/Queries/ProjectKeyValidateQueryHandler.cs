﻿using M4LogCore.Data.MongoDbRepositories;
using M4LogCore.Logic.Common.Interfaces;

namespace M4LogCore.Logic.Repository.Projects.Queries
{
    public class ProjectKeyValidateQueryHandler : IQueryHandler<ProjectKeyValidateQuery, bool>
    {
        private IDbContext _dbContext;

        public ProjectKeyValidateQueryHandler(IDbContext context)
        {
            _dbContext = context;
        }

        public bool Handle(ProjectKeyValidateQuery query)
        {
            return true;
        }
    }
}