﻿using System;
using System.Threading.Tasks;
using M4LogCore.Logic.Common.Enums;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;

namespace M4LogCore.WebApi.Collector.Common.Middlewares
{
    public class GlobalHttpRequestErrorLoggingMiddleware
    {
        private readonly ILogger _logger;
        private readonly RequestDelegate _next;

        public GlobalHttpRequestErrorLoggingMiddleware(ILogger<GlobalHttpRequestErrorLoggingMiddleware> logger, RequestDelegate next)
        {
            _next = next;
            _logger = logger;
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (Exception ex)
            {
                _logger.LogError((int)LoggingEvents.PreRequestProcessing, ex, $"The following error happened: {ex.Message}");
            }
        }
    }
}